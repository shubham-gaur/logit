package logit

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogitFuncName(t *testing.T) {
	type test struct {
		desc string
		req  string
		got  string
		err  error
	}
	var tests []test
	var t1 = test{
		desc: "print all the errors with app name",
		req:  "logit.TestLogitFuncName | ",
		got:  FuncName(),
		err:  nil,
	}
	tests = append(tests, t1)
	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			if !assert.EqualValues(t, tc.got, tc.req) {
				t.Errorf("logit failed got err: %v| got: %v want: %v", tc.err, tc.got, tc.req)
			}
		})
	}
}
